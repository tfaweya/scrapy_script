# Run with a working python3 and scrapy environment
from scrapy import Spider
import re
from csv import writer, DictWriter



class MppaddressesSpider(Spider):
    name = "mppaddresses"
    allowed_domains = ["www.ola.org"]
    start_urls = ['https://www.ola.org/en/members/current']

    with open("menbers.csv", "a") as file:
        csv_writer = writer(file)
        csv_writer.writerow(["Name", "Tel", "Mail"])

    def parse(self, response):
        for url in response.xpath('//td[@headers="view-field-full-name-by-last-name-table-column"]/a/@href').extract()[:10]:
            absolute_url = response.urljoin(url)
            yield scrapy.Request(absolute_url, callback=self.get_details)

    def get_details(self, response):

        dirty_name = response.xpath('//h1[@class="field-content"]/text()').extract()
        name = ''.join(dirty_name).strip()

        dirty_contact = response.xpath('//p[@class="d-inline"]/text()').extract()
        dirty_contact_str = ''.join(dirty_contact)
        regex = '\s[0-9-]+'
        dirty_contact_numbers = re.findall(regex, dirty_contact_str)

        if dirty_contact_numbers[1]:
            tel = dirty_contact_numbers[1]
        else:
            tel = None

        dirty_mail = response.xpath('//p[@class="field-content"]/a/@href').extract_first()
        mail = dirty_mail.replace('mailto:', '')

        with open("menbers.csv", "a") as file:
            csv_writer = writer(file)
            csv_writer.writerow([name, tel, mail])
